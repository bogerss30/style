from django.contrib import admin
from .models import Usuario, Reserva

# Register your models here.

class UsuarioAdmin(admin.ModelAdmin):
    list_display = ['Nombre', 'Apellido','Correo_Electronico','Sexo','Rut']
    search_fields = ['nombre', 'Rut']
    list_filter=['Rut']
    list_per_page = 10

admin.site.register(Usuario)
admin.site.register(Reserva)
