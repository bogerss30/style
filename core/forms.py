from django import forms 
from .models import  Usuario, Reserva
from django.forms import ModelForm
import datetime


class UsuarioForm(ModelForm):

    Nombre= forms.CharField(min_length=4,max_length=20)
    Apellido =forms.CharField(min_length=4,max_length=20)
    Correo_Electronico = forms.CharField(min_length=4,max_length=20)
    Sexo= forms.CharField(min_length=4,max_length=20)
    Rut =  forms.IntegerField(min_value=9 , max_value=9)
    Horario= forms.IntegerField(min_value=1 , max_value=31)

    class Meta:
        model = Usuario
        fields =['Nombre','Apellido','Correo_Electronico','Sexo','Rut','sinopsis']
    
   

class ReservaForm(ModelForm):     
    class Meta2:
        model = Reserva
        fields = ['Horario']
        
        widgets ={
            'Horario':forms.SelectDateWidget(years=range(2020))
            
        }
    def clean_Horario(self):
        Horario = self.cleaned_data['Horario']

        if Horario > datetime.date.today():
            raise forms.ValidationError("La hora no es correcta")

        return Horario
    

        