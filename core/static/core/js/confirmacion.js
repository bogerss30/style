
function confirmarEliminacion(id) {
    Swal.fire({
        title: 'Estas seguro?',
        text: "No podrás deshacer esta acción",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminar!',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.isConfirmed) {
          ///redirigir al usuario a la ruta de eliminar
          window.location.href = "/eliminar-usuario/"+id+"/";
        }
      })
}