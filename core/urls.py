from django.urls import path
from .views import home, galeria, login, listado_usuario, nuevo_usuario, modificar_usuario, eliminar_usuario

urlpatterns = [
    path('', home, name="home"),
    path('galeria/', galeria, name="galeria"),
    path('login/', login, name="login"),
    path('listado-usuarios/', listado_usuario, name="listado_usuarios"),
    path('nuevo-usuario/',nuevo_usuario, name="nuevo_usuario"),
    path('modificar-usuario/<id>/', modificar_usuario, name="modificar_usuario"),
    path('eliminar-usuario/<id>/', eliminar_usuario, name="eliminar_usuario"),
   
]
