from django.shortcuts import render
from .models import Usuario
from .forms import UsuarioForm


# Create your views here.

def home(request):
    
    return render(request, 'core/home.html')


def galeria(request):
    return render(request, 'core/galeria.html')

def login(request):
    return render(request, 'core/login.html')

def listado_usuario(request):
    Usuario = Usuario.objects.all()
    data = {
        'usuarios':Usuario
    }
    return render(request, 'core/listado_usuario.html')

def nuevo_usuario(request):
    data = {
        'form':UsuarioForm()
    }

    if request.method == 'POST':
        formulario = UsuarioForm(request.POST)
        if formulario.is_valid():
            formulario.save()
            data['mensaje'] = "Guardado corretamente"
        data['form']= formulario

    return render(request, 'core/nuevo_usuario.html', data)


def modificar_usuario(request, id):
    Usuario = Usuario.objects.get(id=id)
    data = {
        'form': UsuarioForm(instance=Usuario)
    }

    if request.method == 'POST  ':
        formulario = UsuarioForm(data=request.POST, instance=Usuario)
        if formulario.is_valid():
            formulario.save()
            data['mensaje'] = "Modificado correctamente"
        data['form'] = formulario


    return render(request, 'core/modificar_usuario.html', data)

def eliminar_usuario(request, id):
    Usuario = Usuario.objects.get(id=id)
    Usuario.delete()

    return redirect(to="listado_usuario")




